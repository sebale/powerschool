<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'local_powerschool', language 'en'
 *
 * @package    local_powerschool
 * @copyright  IntelliBoard, Inc <https://intelliboard.net/>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'PowerSchool';
$string['setting_title'] = 'Required settings for plugin';
$string['setting_users'] = 'Settings for sync users';
$string['temp_password'] = 'Temporary password';
$string['temp_password_desc'] = 'Temporary password for new users from PowerSchool';
$string['sync_moodle_user_change'] = 'Sync moodle student change';
$string['sync_moodle_user_change_desc'] = 'When moodle student is changed, changes sync with PowerSchool';
$string['master_url'] = 'PowerSchool site url';
$string['master_url_desc'] = 'Example:"https://partner.powerschool.com"';
$string['client_id'] = 'Client ID';
$string['client_id_desc'] = 'Locate in your site: Start Page > System Administrator > System Settings > Plugin Management Dashboard > Sync Moodle with PowerSchool Setup > Data Configuration for Sync Moodle with PowerSchool ';
$string['client_secret'] = 'Client Secret';
$string['client_secret_desc'] = 'Locate in your site: Start Page > System Administrator > System Settings > Plugin Management Dashboard > Sync Moodle with PowerSchool Setup > Data Configuration for Sync Moodle with PowerSchool ';
$string['school_numbers'] = 'Schools Number';
$string['school_numbers_desc'] = 'Locate in your site: Start Page > District Setup > Schools/School Info, if you have more than 1 record you must list school numbers. Example: "100,200"';
$string['setting_subscription'] = 'Setting subscription for plugin';
$string['user_key'] = 'User key';
$string['secret_key'] = 'Secret key';
$string['course_not_sync'] = 'This course not synced with powerschool, please create course and section in powerschool and go to <a href="'.$CFG->wwwroot.'/local/powerschool/manual-sync.php">Manual Sync</a>';

$string['manual_sync'] = 'Manual Sync';
$string['sync_courses'] = 'Sync Courses';
$string['users_in_school'] = 'In school {$a} users';
$string['users_for_sync'] = 'Sync {$a} users';
$string['powerschoolroot'] = 'PowerSchool';
$string['error_search_school'] = 'School width number {$a} not found';
$string['powerschool_coursecat_desc'] = 'It is category for PowerSchool courses';
$string['terms'] = 'Terms: ';
$string['missingterms'] = 'Missing terms';
$string['users_role'] = 'User roles';
$string['student_role'] = 'Student role';
$string['co_teacher_role'] = 'Co-teacher role';
$string['lead_teacher_role'] = 'Lead teacher role';
$string['attendance'] = 'Attendance';
$string['attendance_code'] = 'Attendance code';
$string['attendance_code_desc'] = 'Admin must create new attendance code for Moodle or use exist attendance code. Attendance codes locate in your site: Start Page > School Setup > Attendance Codes';
$string['attendance_enable'] = 'Attendance enable';
$string['attendance_enable_desc'] = 'Register student activity on course and create PowerSchool attendance';
$string['attendance_time'] = 'Attendance time';
$string['attendance_time_desc'] = 'Time for Powerschool AutoComm Attendance (How to create see file readme.md in plugin package)';
$string['attendance_duration'] = 'Attendance duration';
$string['attendance_duration_desc'] = 'Duration for student activity, powerschool attendance created after this duration';
$string['section_setting'] = 'Powerschool section settings';
$string['section_type'] = 'Section as';
$string['section_type_help'] = 'Powerschool section on course create as moodle course or as user group on moodle course, available only before first sync';
$string['course'] = 'Course';
$string['groups'] = 'User group';
$string['assignment_sync'] = 'Activity sync';


/* name of profile fields */
$string['powerschool_category_name'] = 'Powerschool';
$string['powerschool777id'] = 'Powerschool ID';
$string['powerschool777usertype'] = 'User type';
$string['powerschool777local_id'] = 'Local ID';
$string['powerschool777demographics777gender'] = 'Gender';
$string['powerschool777demographics777birth_date'] = 'Birth Date';
$string['powerschool777demographics777projected_graduation_year'] = 'Projected Graduation Year';
$string['powerschool777addresses777physical777street'] = 'Addresses: Physical Street';
$string['powerschool777addresses777physical777city'] = 'Addresses: Physical City';
$string['powerschool777addresses777physical777state_province'] = 'Addresses: Physical State province';
$string['powerschool777addresses777physical777postal_code'] = 'Addresses: Physical Postal code';
$string['powerschool777addresses777mailing777street'] = 'Addresses: Mailing Street';
$string['powerschool777addresses777mailing777city'] = 'Addresses: Mailing City';
$string['powerschool777addresses777mailing777state_province'] = 'Addresses: Mailing State province';
$string['powerschool777addresses777mailing777postal_code'] = 'Addresses: Mailing Postal code';
$string['powerschool777addresses777mailing777grid_location'] = 'Addresses: Grid Location';
$string['powerschool777alerts777legal777description'] = 'Alerts: Legal Description';
$string['powerschool777alerts777legal777expires_date'] = 'Alerts: Legal Expires date';
$string['powerschool777alerts777discipline777description'] = 'Alerts: Discipline Description';
$string['powerschool777alerts777discipline777expires_date'] = 'Alerts: Discipline Expires date';
$string['powerschool777alerts777medical777description'] = 'Alerts: Medical Description';
$string['powerschool777alerts777medical777expires_date'] = 'Alerts: Medical Expires date';
$string['powerschool777alerts777other777description'] = 'Alerts: Other Description';
$string['powerschool777alerts777other777expires_date'] = 'Alerts: Other Expires date';
$string['powerschool777phones777main777number'] = 'Phones: Main Number';
$string['powerschool777school_enrollment777enroll_status'] = 'School enrollment: Enroll status'; 
$string['powerschool777school_enrollment777enroll_status_description'] = 'School enrollment: Enroll status description';
$string['powerschool777school_enrollment777enroll_status_code'] = 'School enrollment: enroll status code';
$string['powerschool777school_enrollment777grade_level'] = 'School enrollment: grade level';
$string['powerschool777school_enrollment777entry_date'] = 'School enrollment: entry date';
$string['powerschool777school_enrollment777exit_date'] = 'School enrollment: exit date';
$string['powerschool777school_enrollment777school_number'] = 'School enrollment: school number';
$string['powerschool777school_enrollment777school_id'] = 'School enrollment: school id';
$string['powerschool777school_enrollment777entry_code'] = 'School enrollment: entry code';
$string['powerschool777school_enrollment777entry_comment'] = 'School enrollment: entry comment';
$string['powerschool777school_enrollment777track'] = 'School enrollment: track';
$string['powerschool777school_enrollment777full_time_equivalency777fteid'] = 'School enrollment: full time equivalency fteid';
$string['powerschool777school_enrollment777full_time_equivalency777name'] = 'School enrollment: full time equivalency name';
$string['powerschool777school_enrollment777district_of_residence'] = 'School enrollment: district of residence';
$string['powerschool777ethnicity_race777scheduling_reporting_ethnicity'] = 'Ethnicity race: Scheduling reporting ethnicity';
$string['powerschool777contact777emergency_contact_name1'] = 'Contact: emergency contact name1';
$string['powerschool777contact777emergency_contact_name2'] = 'Contact: emergency contact name2';
$string['powerschool777contact777emergency_phone1'] = 'Contact: emergency phone1';
$string['powerschool777contact777emergency_phone2'] = 'Contact: emergency phone2';
$string['powerschool777contact777guardian_email'] = 'Contact: guardian email';
$string['powerschool777contact777mother'] = 'Contact: mother';
$string['powerschool777contact777father'] = 'Contact: father';
$string['powerschool777contact777doctor_name'] = 'Contact: doctor name';
$string['powerschool777contact777doctor_phone'] = 'Contact: doctor phone';
$string['powerschool777contact777guardian_fax'] = 'Contact: guardian fax';
$string['powerschool777contact_info777email'] = 'Contact info: email';
$string['powerschool777initial_enrollment777district_entry_grade_level'] = 'Initial enrollment: district entry grade level';
$string['powerschool777initial_enrollment777school_entry_grade_level'] = 'Initial enrollment: school entry grade level';
$string['powerschool777initial_enrollment777school_entry_date'] = 'Initial enrollment: school entry date';
$string['powerschool777schedule_setup777home_room'] = 'Schedule setup: home room';
$string['powerschool777schedule_setup777next_school'] = 'Schedule setup: next school';
$string['powerschool777schedule_setup777sched_next_year_grade'] = 'Schedule setup: sched next year grade';
$string['powerschool777fees777fee777fee_amount'] = 'Fees Fee: amount';
$string['powerschool777fees777fee777fee_balance'] = 'Fees Fee: balance';
$string['powerschool777fees777fee777fee_description'] = 'Fees Fee: description';
$string['powerschool777fees777fee777fee_paid'] = 'Fees Fee: paid';
$string['powerschool777fees777fee777id'] = 'Fees Fee: ID';
$string['powerschool777fees777fee777transaction_date'] = 'Fees Fee: transaction date';
$string['powerschool777fees777fee777date_created'] = 'Fees Fee: date created';
$string['powerschool777fees777fee777date_modified'] = 'Fees Fee: date modified';
$string['powerschool777fees777fee777category_name'] = 'Fees Fee: category name';
$string['powerschool777fees777fee777type_name'] = 'Fees Fee: type name';
$string['powerschool777fees777fee777type_id'] = 'Fees Fee: type id';
$string['powerschool777fees777fee777priority'] = 'Fees Fee: priority';
$string['powerschool777fees777fee777pro_ratable_indicator'] = 'Fees Fee: pro ratable indicator';
$string['powerschool777fees777fee777group_transaction_id'] = 'Fees Fee: group transaction id';
$string['powerschool777fees777fee777school_id'] = 'Fees Fee: school id';
$string['powerschool777fees777fee777term_id'] = 'Fees Fee: term id';
$string['powerschool777fees777fee777year_id'] = 'Fees Fee: year id';
$string['powerschool777fees777fee777student_id'] = 'Fees Fee: student id';
$string['powerschool777fees777fee777fee_transaction777id'] = 'Fees Fee: fee transaction id';
$string['powerschool777fees777fee777fee_transaction777student_id'] = 'Fees Fee: fee transaction student id';
$string['powerschool777fees777fee777fee_transaction777transaction_amount'] = 'Fees Fee: fee transaction transaction amount';
$string['powerschool777fees777fee777fee_transaction777global_starting_balance'] = 'Fees Fee: fee transaction global starting balance';
$string['powerschool777fees777fee777fee_transaction777date'] = 'Fees Fee: fee transaction date';
$string['powerschool777fees777fee777fee_transaction777description'] = 'Fees Fee: fee transaction description';
$string['powerschool777fees777fee777fee_transaction777group_transaction_id'] = 'Fees Fee: fee transaction group transaction id';
$string['powerschool777fees777fee777fee_transaction777school_id'] = 'Fees Fee: fee transaction school id';
$string['powerschool777fees777fee777fee_transaction777start_year'] = 'Fees Fee: fee transaction start year';
$string['powerschool777fees777fee777fee_transaction777starting_balance'] = 'Fees Fee: fee transaction starting balance';
$string['powerschool777fees777fee777fee_transaction777global_net_balance'] = 'Fees Fee: fee transaction global net balance';
$string['powerschool777fees777fee777fee_transaction777transaction_type'] = 'Fees Fee: fee transaction type';
$string['powerschool777fees777fee_balance777id'] = 'Fees: fee balance id';
$string['powerschool777fees777fee_balance777student_id'] = 'Fees: fee balance student id';
$string['powerschool777fees777fee_balance777year_id'] = 'Fees: fee balance year id';
$string['powerschool777fees777fee_balance777debit'] = 'Fees: fee balance debit';
$string['powerschool777fees777fee_balance777credit'] = 'Fees: fee balance credit';
$string['powerschool777fees777fee_balance777school_id'] = 'Fees: fee balance school id';
$string['powerschool777fees777fee_balance777balance'] = 'Fees: fee balance ';
$string['powerschool777fees777fee_balance777fee_exemption_status'] = 'Fees: fee balance exemption status';
$string['powerschool777fees777fee_balance777fee_exemption_status_code'] = 'Fees: fee balance exemption status code';
$string['powerschool777lunch777balance_1'] = 'Lunch: balance 1';
$string['powerschool777lunch777balance_2'] = 'Lunch: balance 2';
$string['powerschool777lunch777balance_3'] = 'Lunch: balance 3';
$string['powerschool777lunch777balance_4'] = 'Lunch: balance 4';
$string['powerschool777lunch777last_meal'] = 'Lunch: meal';
$string['powerschool777lunch777lunch_id'] = 'Lunch: id';
$string['powerschool777addresses777home777street'] = 'Addresses: home street';
$string['powerschool777addresses777home777state_province'] = 'Addresses: home state/province';
$string['powerschool777addresses777home777postal_code'] = 'Addresses: home postal code';
$string['powerschool777addresses777home777city'] = 'Addresses: home city';
$string['powerschool777school_affiliations777school_affiliation777school_id'] = 'School affiliation: school_id';
$string['powerschool777school_affiliations777school_affiliation777type'] = 'School affiliation: type';
$string['powerschool777school_affiliations777school_affiliation777status'] = 'School affiliation: status';
$string['powerschool777demographics777district_entry_date'] = 'Demographics: district entry date';
$string['powerschool777initial_enrollment777district_entry_date'] = 'Initial enrollment: district entry date';
$string['powerschool777phones777home_phone'] = 'Phones: home';
$string['powerschool777addresses777physical777grid_location'] = 'Addresses: physical grid location';
$string['powerschool777ethnicity_race777federal_ethnicity'] = 'Ethnicity race: federal ethnicity';
$string['powerschool777state_province_id'] = 'State province id';
$string['powerschool777unique_id'] = 'Unique id';
$string['powerschool777school_id'] = 'School id';
/* name of profile fields */

$string['include_expansions'] = 'Include Expansions';
$string['include_extensions'] = 'Include Extensions';
$string['demographics'] = 'Demographics';
$string['addresses'] = 'Addresses';
$string['alerts'] = 'Alerts';
$string['phones'] = 'Phones';
$string['school_enrollment'] = 'School enrollment';
$string['ethnicity_race'] = 'Ethnicity race';
$string['contact'] = 'Contact';
$string['contact_info'] = 'Contact info';
$string['initial_enrollment'] = 'Initial enrollment';
$string['schedule_setup'] = 'Schedule setup';
$string['fees'] = 'Fees';
$string['lunch'] = 'Lunch';
$string['activities'] = 'Activities';
$string['c_studentlocator'] = 'Student locator';
$string['u_students_extension'] = 'Students extension';
$string['u_bus'] = 'Bus';
$string['sync'] = 'Sync';
$string['include_users'] = 'Include Users';
$string['students'] = 'Students';
$string['staffs'] = 'Staffs';
$string['emails'] = 'Emails';
$string['school_affiliations'] = 'School affiliations';
$string['processing'] = 'Processing...';
$string['must_exist_students_or_staffs'] = 'Must be included students or staffs';
$string['sync_error'] = 'Sync Error';
$string['sync_error_desc'] = 'Please reload page after 1 minute or contact to administrator';
$string['associate_category'] = 'Associate category ';
$string['associate_category_full'] = 'Associate PowerSchool category to Moodle course module (only for new PowerSchool Assignments)';
$string['not_set'] = 'Not Set';
$string['school_category'] = 'Course category for school';
$string['courses_sync_per_school'] = 'Courses sync for school: {$a}';
$string['sync_new_ps_moodle_group'] = 'Sync new assignments from Powerschool section: {$a}';
$string['sync_new_ps_moodle'] = 'Sync new assignments from Powerschool section';
$string['associate_moodle_user'] = 'Association users';
$string['associate_moodle_user_desc'] = 'Synchronization new users on email or username';
$string['ps_grade_scale'] = 'Grade scale from PowerSchool';
$string['save'] = 'Save Only';
$string['save_sync_all'] = 'Save and Sync all data';
$string['save_sync_users'] = 'Save and Sync users';
$string['save_sync_courses'] = 'Save and Sync courses';
$string['terms_sync_per_school'] = 'Terms sync per school: {$a}';










