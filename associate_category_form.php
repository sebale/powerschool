<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * PowerSchool Settings
 *
 * @package    local_powerschool
 * @copyright  IntelliBoard, Inc <https://intelliboard.net/>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    //  It must be included from a Moodle page.
}

require_once($CFG->dirroot.'/lib/formslib.php');

class associate_category_form extends moodleform {

    /**
     * Define the form.
     */
    public function definition () {
        global $DB;
        $courseid = $this->_customdata['courseid'];
        $plugin = $this->_customdata['instance'];
        $mform = $this->_form;


        if($plugin->enable_groups){
            $sections = $DB->get_records_sql('SELECT cf.section_id, g.name, cf.gradebooktype, cf.section_dcid, cf.teacher_user_dcid, cf.term_id
                                             FROM {powerschool_course_fields} cf 
                                                LEFT JOIN {powerschool_course_cat} cc ON cc.courseid_group=cf.courseid AND cc.courseid=cf.section_id
                                                LEFT JOIN {groups} g ON g.id=cc.catid
                                             WHERE cf.courseid=:courseid', array('courseid' => $courseid));

            $elements = array();
            foreach($sections as $section){
                $element = array('name'=>$section->name,'categories'=>array(),'section_id'=>$section->section_id);
                if($section->gradebooktype == 1){
                    $data = new stdClass();
                    $data->section_id = $section->section_dcid;
                    $data = json_encode($data);

                    $response = $plugin->request('post', '/ws/schema/query/get.section.gradebook.cat', array(), $data);
                    foreach($response->record as $item){
                        $element['categories'][] = $item->tables->pgcategories;
                    }
                }elseif($section->gradebooktype == 2){
                    $response = $plugin->request('get', '/ws/xte/teacher_category?users_dcid='.$section->teacher_user_dcid.'&term_id='.$section->term_id);
                    foreach($response as $item){
                        $category = new stdClass();
                        $category->categoryid = $item->_id;
                        $category->name = $item->name;
                        $element['categories'][] = $category;
                    }
                }
                $elements[] = $element;
            }

        }else{
            $elements = array();
            $section = $DB->get_record('powerschool_course_fields', array('courseid' => $courseid));

            $element = array('categories'=>array(),'section_id'=>$section->section_id);
            if($section->gradebooktype == 1){
                $data = new stdClass();
                $data->section_id = $section->section_dcid;
                $data = json_encode($data);

                $response = $plugin->request('post', '/ws/schema/query/get.section.gradebook.cat', array(), $data);
                foreach($response->record as $item){
                    $element['categories'][] = $item->tables->pgcategories;
                }
            }elseif($section->gradebooktype == 2){
                $response = $plugin->request('get', '/ws/xte/teacher_category?users_dcid='.$section->teacher_user_dcid.'&term_id='.$section->term_id);
                foreach($response as $item){
                    $category = new stdClass();
                    $category->categoryid = $item->_id;
                    $category->name = $item->name;
                    $element['categories'][] = $category;
                }
            }
            $elements[] = $element;
        }

        $modules = array(''=>get_string('not_set','local_powerschool'));
        $modules = array_merge($modules, get_module_types_names());

        foreach($elements as $element){
            if(isset($element['name']))
                $mform->addElement('header', 'moodle', $element['name']);

            $isset_categories_obj = $DB->get_records('powerschool_assignment_cat',array('section_id'=>$element['section_id']));

            $isset_categories = array();
            foreach($isset_categories_obj as $item){
                $isset_categories[$item->power_cat] = $item->moodle_mod;
            }

            foreach($element['categories'] as $category){
                $mform->addElement('select', $category->categoryid.'_'.$element['section_id'], $category->name, $modules);

                if(isset($isset_categories[$category->categoryid])){
                    $mform->setDefault($category->categoryid . '_' . $element['section_id'], $isset_categories[$category->categoryid]);
                }
            }
        }



        $this->add_action_buttons();
    }

}


