 <?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package    local_powerschool
 * @copyright  IntelliBoard, Inc <https://intelliboard.net/>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/local/powerschool/locallib.php');
require_once($CFG->dirroot.'/user/profile/definelib.php');
require_once($CFG->dirroot.'/user/lib.php');

class Users extends PowerSchool{
	private $profile_fields = array();
	private $exist_users = array();
	private $fieldname_separator = '777';
	private $profile_category;
	private $field_sortorder = 0;
	private $field_need_reorder = 0;
	private $temp_pass = 0;

	public function __construct(){
		parent::__construct();

		global $DB;

		$data = $DB->get_records('user_info_field',null,'','id,shortname');
		foreach($data as $item){
			$this->profile_fields[$item->shortname] = $item->id;
		}
		$this->field_sortorder = count($data)+1;

		if($DB->record_exists('user_info_category',array('id'=>get_config('local_powerschool', 'profile_category')))){
			$this->profile_category = get_config('local_powerschool', 'profile_category');
		}else{
			$new_categoty = new stdClass();
			$new_categoty->name = get_string('powerschool_category_name', 'local_powerschool');
			$new_categoty->sortorder = 99999;
			$this->profile_category = $DB->insert_record('user_info_category', $new_categoty);
			set_config('profile_category',$this->profile_category,'local_powerschool');
			profile_reorder_categories();
		}

		$this->exist_users = $DB->get_records_sql("SELECT u.id, CONCAT(d.data,dl.data) as powerid, u.username, u.email
										FROM {user} u
										  LEFT JOIN {user_info_field} f ON f.categoryid={$this->profile_category} AND f.shortname='powerschool{$this->fieldname_separator}usertype'
										  LEFT JOIN {user_info_data} d ON d.userid=u.id AND d.fieldid=f.id
										  LEFT JOIN {user_info_field} fl ON fl.categoryid={$this->profile_category} AND fl.shortname='powerschool{$this->fieldname_separator}local_id'
										  LEFT JOIN {user_info_data} dl ON dl.userid=u.id AND dl.fieldid=fl.id
										WHERE u.deleted=0");

		$this->temp_pass = get_config('local_powerschool', 'temp_password');
	}

	public function sync_users($page,$pagesize = 0){
		$params = json_decode(get_config('local_powerschool', 'sync_params'));
		if($pagesize == 0){
			$pagesize = min($this->metadata->metadata->student_max_page_size,$this->metadata->metadata->staff_max_page_size);
		}

		foreach($this->school_ids as $school_id){
            $expansions = array();
            if(isset($params->students)){
                foreach($params as $param => $value){
                    if(strpos($param, 'udent_expansions_') && $value == 1){
                        $expansions[] = str_replace('student_expansions_', '', $param);
                    }
                }
                $expansions = (!empty($expansions))?'&expansions=' . implode(',', $expansions):'';

                $data = $this->request('get', "/ws/v1/school/{$school_id}/student?page={$page}&pagesize=$pagesize" . $expansions);

                if(isset($data->students->student)){
                    if(is_array($data->students->student)){
                        foreach($data->students->student as $item){
                            $item->usertype = 'student';
                            $item->school_id = $school_id;
                            $this->execute_user($item);
                        }
                    }else{
                        $item = $data->students->student;
                        $item->usertype = 'student';
                        $item->school_id = $school_id;
                        $this->execute_user($item);
                    }
                }
            }

            if(isset($params->staffs)){
                $expansions = array();
                foreach($params as $param => $value){
                    if(strpos($param, 'taff_expansions_') && $value == 1){
                        $expansions[] = str_replace('staff_expansions_', '', $param);
                    }
                }
                $expansions[] = 'school_affiliations';
                $expansions = (!empty($expansions))?'&expansions=' . implode(',', $expansions):'';

                $data = $this->request('get', "/ws/v1/school/{$school_id}/staff?page={$page}&pagesize=$pagesize" . $expansions);

                $staff = array();
                if(isset($data->staffs->staff)){
                    if(is_array($data->staffs->staff)){
                        foreach($data->staffs->staff as $item){
                            if(is_array($item->school_affiliations->school_affiliation)){
                                foreach($item->school_affiliations->school_affiliation as $school_affiliation){
                                    if(in_array($school_affiliation->school_id, $this->school_ids) && (!empty($item->teacher_username) || !empty($item->admin_username))){
                                        unset($item->school_affiliations);
                                        $staff[] = $item;
                                    }
                                }
                            }else{
                                if(in_array($item->school_affiliations->school_affiliation->school_id,$this->school_ids) && (!empty($item->teacher_username) || !empty($item->admin_username))){
                                    unset($item->school_affiliations);
                                    $staff[] = $item;
                                }
                            }
                        }

                        foreach($staff as $item){
                            $item->usertype = 'staff';
                            $item->school_id = $school_id;
                            $this->execute_user($item);
                        }
                    }else{
                        $item = $data->staffs->staff;
                        if(is_array($item->school_affiliations->school_affiliation)){
                            foreach($item->school_affiliations->school_affiliation as $school_affiliation){
                                if(in_array($school_affiliation->school_id,$this->school_ids) && (!empty($item->teacher_username) || !empty($item->admin_username))){
                                    unset($item->school_affiliations);
                                    $staff = $item;
                                }
                            }
                        }else{
                            if(in_array($item->school_affiliations->school_affiliation->school_id,$this->school_ids) && (!empty($item->teacher_username) || !empty($item->admin_username))){
                                unset($item->school_affiliations);
                                $staff = $item;
                            }
                        }
                        $staff->usertype = 'staff';
                        $staff->school_id = $school_id;
                        $this->execute_user($staff);
                    }
                }
            }
        }
		
	}
	
	public function execute_user($item){
		$username = $this->get_power_username($item);

		if(empty($username)){
			return;
		}

		$id = $this->exist_user($item);
		if($id){
			$student = new stdClass();
			$student->id = $id;
			$student->username = $username;
			$student->firstname = (!empty($item->name->first_name))?$item->name->first_name:'';
			$student->lastname = (!empty($item->name->last_name))?$item->name->last_name:'';
			$student->email = $this->get_power_email($item);
			user_update_user($student, false, false);
			$this->execute_user_profile($student->id,$item);
		}else{
			$student = new stdClass();
			$student->username = $username;
			$student->firstname = (!empty($item->name->first_name))?$item->name->first_name:'';
			$student->lastname = (!empty($item->name->last_name))?$item->name->last_name:'';
			$student->password = $this->temp_pass;
			$student->confirmed = 1;
			$student->mnethostid = 1;
			$student->email = $this->get_power_email($item);
			$new_user_id = user_create_user($student);
			
			set_user_preference('auth_forcepasswordchange', true, $new_user_id);
			$this->execute_user_profile($new_user_id,$item);

            $exist_user = new stdClass();
            $exist_user->id = $new_user_id;
            $exist_user->powerid = $item->usertype.$item->local_id;
            $exist_user->username = $username;
            $exist_user->email = $this->get_power_email($item);
            $this->exist_users[] = $exist_user;
		}
		if($this->field_need_reorder){
			profile_reorder_fields();
		}
	}

	private function exist_user($power_user){
	    $sync_on = get_config('local_powerschool','associate_moodle_user');
		$powerid = $power_user->usertype.$power_user->local_id;

		foreach($this->exist_users as $exist_user){
			if($exist_user->powerid == $powerid)
				return $exist_user->id;
		}
		if($sync_on == 'email'){
            foreach($this->exist_users as $exist_user){
                if($exist_user->email == $this->get_power_email($power_user) && $exist_user->email != 'not_exist@example.com')
                    return $exist_user->id;
            }
        }elseif($sync_on == 'username'){
            foreach($this->exist_users as $exist_user){
                if($exist_user->username == $this->get_power_username($power_user))
                    return $exist_user->id;
            }
        }

		return false;
	}

	private function get_power_username($power_user){
		$username = (isset($power_user->student_username))?core_text::strtolower($power_user->student_username):'';
		$username = (isset($power_user->teacher_username))?core_text::strtolower($power_user->teacher_username):$username;
		$username = (isset($power_user->admin_username))?core_text::strtolower($power_user->admin_username):$username;

		return $username;
	}

	private function get_power_email($power_user){
		$email = 'not_exist@example.com';
		if(!empty($power_user->contact_info->email)){
			$email = $power_user->contact_info->email;
		}elseif(!empty($power_user->contact->guardian_email)){
			$email = $power_user->contact->guardian_email;
		}elseif(!empty($power_user->emails->work_email)){
			$email = $power_user->emails->work_email;
		}

		return $email;
	}
	
	private function execute_user_profile($user_id, $fields, $field_shortname = 'powerschool'){
		global $DB;
		
		foreach($fields as $name=>$item){
			if(in_array($name,array('teacher_username','student_username','name','admin_username','emails')) && $field_shortname == 'powerschool') 
				continue;
			if(empty($item) && $item !== 0) 
				continue;
			
			if(is_object($item) || is_array($item)){
				$this->execute_user_profile($user_id, $item, $field_shortname.$this->fieldname_separator.$name);
				continue;
			}
			
			if(isset($this->profile_fields[$field_shortname.$this->fieldname_separator.$name])){
				if($id = $DB->get_record('user_info_data',array('userid'=>$user_id,'fieldid'=>$this->profile_fields[$field_shortname.$this->fieldname_separator.$name]),'id')){
					$field_data = new stdClass();
					$field_data->id = $id->id;
					$field_data->data = $item;
					$DB->update_record('user_info_data',$field_data);
				}else{
					$field_data = new stdClass();
					$field_data->userid = $user_id;
					$field_data->fieldid = $this->profile_fields[$field_shortname.$this->fieldname_separator.$name];
					$field_data->data = $item;
					$DB->insert_record('user_info_data',$field_data);
				}
			}else{
				$new_field = new stdClass();
				$new_field->shortname = $field_shortname.$this->fieldname_separator.$name;
				$new_field->name = get_string($field_shortname.$this->fieldname_separator.$name, 'local_powerschool');
				$new_field->datatype = 'text';
				$new_field->descriptionformat = 1;
				$new_field->categoryid = $this->profile_category;
				$new_field->sortorder = $this->field_sortorder++;
				$new_field->locked = 1;
				$new_field->visible = (in_array($field_shortname.$this->fieldname_separator.$name,array('powerschool777id','powerschool777local_id','powerschool777usertype','powerschool777school_id')))?0:1;
				$new_field->forceunique = 0;
				$new_field->signup = 0;
				$new_field->defaultdataformat = 0;
				$new_field->param1 = 30; //??
				$new_field->param2 = 2048;
				$new_field->param3 = 0;
				$this->profile_fields[$field_shortname.$this->fieldname_separator.$name] = $DB->insert_record('user_info_field',$new_field);	
				$this->field_need_reorder = 1;
				
				$field_data = new stdClass();
				$field_data->userid = $user_id;
				$field_data->fieldid = $this->profile_fields[$field_shortname.$this->fieldname_separator.$name];
				$field_data->data = $item;
				$DB->insert_record('user_info_data',$field_data);
			}
		}
		
	}
	
	public function sync_moodle_user($user){
		global $DB;

		$profile_fields = $DB->get_records_sql('SELECT f.id, f.shortname, d.data
												FROM {user_info_data} d
													JOIN {user_info_field} f ON f.id=d.fieldid AND f.categoryid = :categoryid
												WHERE d.userid=:userid
											',array('categoryid'=>$this->profile_category,'userid'=>$user->id));

		$field_obj = new stdClass();
		foreach($profile_fields as $field){
			if($field->shortname == 'powerschool'.$this->fieldname_separator.'usertype' && $field->data != 'student')
				return;

			$this->get_object_from_path(explode($this->fieldname_separator, $field->shortname),$field_obj, $field->data);
		}
		if(!isset($field_obj->powerschool->local_id))
			return;

		$field_obj->powerschool->name->first_name = $user->firstname;
		$field_obj->powerschool->name->last_name = $user->lastname;
		$field_obj = $this->clear_profile($field_obj,$user);

		$request = new stdClass();
		@$request->students->student = $field_obj;
		$request->students->student->action = 'UPDATE';
		$request->students->student->client_uid = $field_obj->id;

		$response = $this->request('post','/ws/v1/student', array(),json_encode($request));

		if($response->results->result->status == 'SUCCESS'){
			return true;
		}else{
			return $response;
		}
	}

	private function get_object_from_path($path, &$obj, $data){
		if(empty($path)) {
			$obj = $data;
		}elseif(is_array($path)) {
			$item = array_shift($path);
			$this->get_object_from_path($path,$obj->$item,$data);
		}
	}

	private function clear_profile($field_obj,$user){
		$field_obj = $field_obj->powerschool;

		if(count($field_obj->addresses->physical)<4){
			unset($field_obj->addresses->physical);
		}
		if(count($field_obj->addresses->home)<4){
			unset($field_obj->addresses->home);
		}
		if(count($field_obj->addresses->mailing)<4){
			unset($field_obj->addresses->mailing);
		}
		if(count($field_obj->schedule_setup)<2){
			unset($field_obj->schedule_setup);
		}
		unset($field_obj->lunch);
		unset($field_obj->local_id);
		unset($field_obj->school_enrollment);
		unset($field_obj->usertype);

		if(empty($field_obj->contact_info->email)){
			$field_obj->contact_info->email = $user->email;
		}

		return $field_obj;
	}
}
