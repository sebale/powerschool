<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * PowerSchool Settings
 *
 * @package    local_powerschool
 * @copyright  IntelliBoard, Inc <https://intelliboard.net/>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

function xmldb_local_powerschool_upgrade($oldversion) {
    global $CFG, $DB;

    $dbman = $DB->get_manager();
    if($oldversion < 2016071800){
        $table = new xmldb_table('powerschool_assignment_cat');
        $field = new xmldb_field('grade_cat');
        $dbman->drop_field($table, $field);

        $field = new xmldb_field('section_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $dbman->add_field($table, $field);
        $field = new xmldb_field('moodle_mod', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $dbman->add_field($table, $field);
        $field = new xmldb_field('timecreate', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $dbman->add_field($table, $field);

        $table = new xmldb_table('powerschool_course_fields');
        $field = new xmldb_field('gradebooktype', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $dbman->add_field($table, $field);
        $field = new xmldb_field('section_dcid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $dbman->add_field($table, $field);
        $field = new xmldb_field('section_id', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $dbman->add_field($table, $field);
        $field = new xmldb_field('term_id', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $dbman->add_field($table, $field);
    }
    if($oldversion < 2016072700){
        if(!$dbman->field_exists('powerschool_course_fields', 'gradescaleitem')){
            $table = new xmldb_table('powerschool_course_fields');
            $field = new xmldb_field('gradescaleitem', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
            $dbman->add_field($table, $field);
        }

        if(!$dbman->field_exists('powerschool_assignment', 'assignmentsectionid')){
            $table = new xmldb_table('powerschool_assignment');
            $field = new xmldb_field('assignmentsectionid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
            $dbman->add_field($table, $field);
        }
    }
    if($oldversion < 2016081000){
        $table = new xmldb_table('powerschool_course_fields');
        $field = new xmldb_field('section_wheretaught', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $dbman->change_field_type($table, $field);
        $field = new xmldb_field('section_wheretaughtdistrict', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $dbman->change_field_type($table, $field);

        $table = new xmldb_table('powerschool_grade_cat');
        $field = new xmldb_field('id', XMLDB_TYPE_INTEGER, '10',null,null,true,null);
        $table->addField($field);
        $field = new xmldb_field('section_id', XMLDB_TYPE_INTEGER, '10',null,null,null,null);
        $table->addField($field);
        $field = new xmldb_field('power_cat', XMLDB_TYPE_INTEGER, '10',null,null,null,null);
        $table->addField($field);
        $field = new xmldb_field('moodle_cat', XMLDB_TYPE_INTEGER, '10',null,null,null,null);
        $table->addField($field);
        $field = new xmldb_field('timecreate', XMLDB_TYPE_INTEGER, '10',null,null,null,null);
        $table->addField($field);

        $key = new xmldb_key('primary');
        $key->setType(XMLDB_KEY_PRIMARY);
        $key->setFields(array('id'));
        $key->setLoaded(true);
        $key->setChanged(true);
        $table->addKey($key);

        $dbman->create_table($table);
    }

    return true;
}