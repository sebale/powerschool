<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * PowerSchool Settings
 *
 * @package    local_powerschool
 * @copyright  IntelliBoard, Inc <https://intelliboard.net/>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require_once('classes/Courses.php');

require_login();
require_capability('local/powerschool:view', context_system::instance());

$page = optional_param('page', 0, PARAM_INT);

$plugin = new Courses();

$page_size = min($plugin->metadata->metadata->course_max_page_size,ceil(30/count($plugin->school_numbers)));

if($page>0){
    $timestart = time();
    $plugin->sync_courses($page,$page_size);
    die(time()-$timestart.' sec');
}

$count = 0;
foreach($plugin->school_numbers as $school_number){
    $count = max($plugin->request('get','/ws/schema/table/sections/count?q=schoolid=='.$school_number)->count,$count);
}
$pages = ceil($count/$page_size);
$pages = ($pages>0)?$pages:1;

$PAGE->set_url(new moodle_url("/local/powerschool/sync-courses.php"));
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title(get_string('powerschoolroot', 'local_powerschool'));
$PAGE->set_heading(get_string('powerschoolroot', 'local_powerschool'));
$PAGE->requires->js('/local/powerschool/javascript/jquery-2.2.0.min.js',true);

echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('sync_courses', 'local_powerschool'));
?>
    <div id="pbar" class="progressbar_container"> 
        <h2><?php echo get_string('processing','local_powerschool');?></h2>
        <div class="progress progress-striped active">
            <div aria-valuenow="0" aria-valuemax="100" aria-valuemin="0" role="progressbar" class="bar" style="width:0;">0%</div>
        </div>
        <p></p>
    </div>
    <script>
        var PowerschoolFailedRequestCounter = 0;
        function send_request(page){
            jQuery.ajax({
                    method: "POST",
                    url: "<?php echo new moodle_url($PAGE->url);?>",
                    data: { page: page}
                })
                .done(function( msg ) {
                    if(page < <?php echo $pages;?>){
                        updateProgressBar("pbar", Math.round((page/<?php echo $pages;?>)*100), "<?php echo get_string('processing','local_powerschool');?>", null);
                        send_request(++page);
                    }else{
                        updateProgressBar("pbar", Math.round((page/<?php echo $pages;?>)*100), "<?php echo get_string('success');?>", null);
                        jQuery('#pbar').after('<?php echo $OUTPUT->continue_button(new moodle_url('/local/powerschool/manual-sync.php'));?>');
                    }
                    PowerschoolFailedRequestCounter = 0;
                }).fail(function (jqXHR, textStatus) {
                    PowerschoolFailedRequestCounter++;
                    if(PowerschoolFailedRequestCounter<3){
                        send_request(page);
                    }else{
                        updateProgressBar("pbar", Math.round((--page/<?php echo $pages;?>)*100), "<?php echo get_string('sync_error','local_powerschool');?>", "<?php echo get_string('sync_error_desc','local_powerschool');?>");
                    }

                });
        }
        updateProgressBar("pbar", 1, "<?php echo get_string('processing','local_powerschool');?>", null);
        jQuery(window).ready(function(){
            send_request(1);
        });

    </script>

<?php
echo $OUTPUT->footer();
