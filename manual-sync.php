<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * PowerSchool Settings
 *
 * @package    local_powerschool
 * @copyright  IntelliBoard, Inc <https://intelliboard.net/>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
 
require('../../config.php');
require_once('classes/Users.php');
require_once('sync_form.php');

require_login();
require_capability('local/powerschool:view', context_system::instance());

$page = optional_param('page', 0, PARAM_INT);

$plugin = new Users();
$min_pagesize = min($plugin->metadata->metadata->student_max_page_size,$plugin->metadata->metadata->staff_max_page_size,ceil(30/count($plugin->school_numbers)));

if($page>0){
	$timestart = time();
	$plugin->sync_users($page,$min_pagesize);
	die(time()-$timestart.' sec');
}

$PAGE->set_url(new moodle_url("/local/powerschool/manual-sync.php"));
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title(get_string('powerschoolroot', 'local_powerschool'));
$PAGE->set_heading(get_string('powerschoolroot', 'local_powerschool'));
$PAGE->requires->js('/local/powerschool/javascript/jquery-2.2.0.min.js',true);

$userform = new sync_users_form(new moodle_url($PAGE->url));
$show_progress = 0;


if ($params = $userform->get_data()) {
	$params->sync_courses = array();
	$params->sync_terms = array();
	foreach($params as $param => $value){
		if(strpos($param, 'courses_') !== false){
			$params->sync_courses[(string)str_replace('courses_', '', $param)] = $value;
			unset($params->$param);
		}elseif(strpos($param, 'terms_') !== false){
			$term = explode('_',(string)str_replace('terms_', '', $param));
			$params->sync_terms[$term[0]][$term[1]] = $value;
			unset($params->$param);
		}
	}

	set_config('sync_params',json_encode($params),'local_powerschool');
	if(isset($params->save))
		redirect(new moodle_url("/local/powerschool/manual-sync.php"));
	elseif(isset($params->save_sync_courses))
		redirect(new moodle_url("/local/powerschool/sync-courses.php"));

	$count_students = $count_staffs = 0;
	foreach($plugin->school_ids as $school_id){
		$count = (isset($params->students))?$plugin->request('get', "/ws/v1/school/{$school_id}/student/count")->resource->count:0;
		$count_students = max($count,$count_students);
		$count = (isset($params->staffs))?$plugin->request('get', "/ws/v1/school/{$school_id}/staff/count")->resource->count:0;
		$count_staffs = max($count,$count_staffs);
	}
	$count_users = max($count_students,$count_staffs);
	
	$pages = ceil($count_users/$min_pagesize);
	$pages = ($pages>0)?$pages:1;
	$show_progress = 1;
	
}else{
	$count_users = 0;
	foreach($plugin->school_ids as $school_id){
		$count_users += $plugin->request('get', "/ws/v1/school/{$school_id}/student/count")->resource->count;
		$count_users += $plugin->request('get', "/ws/v1/school/{$school_id}/staff/count")->resource->count;
	}
	$plugin->create_subscribe();
}


echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('manual_sync', 'local_powerschool'));

if(!$show_progress){
	echo html_writer::tag('h3',get_string('users_in_school', 'local_powerschool', $count_users));
	$userform->display();
}
if($show_progress):
?>
<div id="pbar" class="progressbar_container">
	<h2><?php echo get_string('processing','local_powerschool');?></h2>
	<div class="progress progress-striped active">
		<div aria-valuenow="0" aria-valuemax="100" aria-valuemin="0" role="progressbar" class="bar" style="width:0;">0%</div>
	</div>
	<p></p>
</div>
<script>
	var PowerschoolFailedRequestCounter = 0;
	function send_request(page){
		jQuery.ajax({
		  method: "POST",
		  url: "<?php echo new moodle_url($PAGE->url);?>",
		  data: { page: page}
		})
		  .done(function( msg ) {
			if(page < <?php echo $pages;?>){
				updateProgressBar("pbar", Math.round((page/<?php echo $pages;?>)*100), "<?php echo get_string('processing','local_powerschool');?>", null);
				send_request(++page);
			}else{
				updateProgressBar("pbar", Math.round((page/<?php echo $pages;?>)*100), "<?php echo get_string('success');?>", null);
				<?php if(isset($params->save_sync_users)):?>
					jQuery('#pbar').after('<?php echo $OUTPUT->continue_button(new moodle_url('/local/powerschool/manual-sync.php'));?>');
				<?php else: ?>
					jQuery('#pbar').after('<?php echo $OUTPUT->continue_button(new moodle_url('/local/powerschool/sync-courses.php'));?>');
					window.location.href = '<?php echo new moodle_url('/local/powerschool/sync-courses.php');?>';
				<?php endif; ?>
			}
		  }).fail(function (jqXHR, textStatus) {
				PowerschoolFailedRequestCounter++;
				if(PowerschoolFailedRequestCounter<3){
					send_request(page);
				}else{
					updateProgressBar("pbar", Math.round((--page/<?php echo $pages;?>)*100), "<?php echo get_string('sync_error','local_powerschool');?>", "<?php echo get_string('sync_error_desc','local_powerschool');?>");
				}

			});
	}
	updateProgressBar("pbar", 1, "<?php echo get_string('processing','local_powerschool');?>", null);
	jQuery(window).ready(function(){
		send_request(1);
	});
</script>
<?php endif;?>

<?php
echo $OUTPUT->footer();
