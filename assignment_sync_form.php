<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * PowerSchool Settings
 *
 * @package    local_powerschool
 * @copyright  IntelliBoard, Inc <https://intelliboard.net/>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    //  It must be included from a Moodle page.
}

require_once($CFG->dirroot.'/lib/formslib.php');
require_once($CFG->dirroot.'/local/powerschool/classes/Courses.php');

class assignment_sync_form extends moodleform {

    protected $plugin;
    protected $modules;
    protected $modinfo;
    protected $ps_categoties;

    /**
     * Define the form.
     */
    public function definition () {
        global $DB;
        $courseid = $this->_customdata;
        $mform = $this->_form;
        $plugin = $this->plugin = new Courses();
        $this->modules = array_merge(array(''=>get_string('not_set','local_powerschool')), get_module_types_names());

        $data = $DB->get_records('powerschool_assignment_sync',array('courseid'=>$courseid));
        $this->modinfo = get_fast_modinfo($courseid);

        if($plugin->enable_groups){
            $sections = $DB->get_records_sql('SELECT cf.section_id, g.name, cf.gradebooktype, cf.section_dcid, cf.teacher_user_dcid, cf.term_id, "1" as group_mode
                                             FROM {powerschool_course_fields} cf 
                                                LEFT JOIN {powerschool_course_cat} cc ON cc.courseid_group=cf.courseid AND cc.courseid=cf.section_id
                                                LEFT JOIN {groups} g ON g.id=cc.catid
                                             WHERE cf.courseid=:courseid', array('courseid' => $courseid));
        }else{
            $sections = array();
            $sections[] = $DB->get_record('powerschool_course_fields', array('courseid' => $courseid));
        }

        foreach($sections as $section){
            if($section->gradebooktype != 2)
                continue;

            if(!isset($this->ps_categoties[$section->teacher_user_dcid])){
                $response = $plugin->request('get', '/ws/xte/teacher_category?users_dcid='.$section->teacher_user_dcid.'&term_id='.$section->term_id, array(),'', true);
                if(empty($response[0]))
                    continue;

                $categories = array();
                foreach($response[0] as $item){
                    $categories[$item->_id] = $item->name;
                }
                $this->ps_categoties[$section->teacher_user_dcid] = $categories;
            }
        }


        foreach ($data as $ass){
            $this->get_sync_form_elements($ass);
        }
        $mform->addElement('hidden', 'mod_sync','');
        $mform->setType('mod_sync', PARAM_RAW);

        foreach($sections as $section){
            $this->show_section_new_assignment($section);
        }

        $mform->addElement('hidden', 'new_from_ps','');
        $mform->setType('new_from_ps', PARAM_RAW);

        $mform->addElement('hidden', 'mod_cat','');
        $mform->setType('mod_cat', PARAM_RAW);

        $this->add_action_buttons(true, get_string('sync', 'local_powerschool'));
    }

    private function show_section_new_assignment($section){
        global $DB;
        $mform = $this->_form;
        $plugin = $this->plugin;

        $new_assignments = array();
        $today = date('Y-m-d');
        if($section->gradebooktype == 1){
            $pages = ceil($plugin->request('get', '/ws/schema/table/pgassignments/count?q=sectionid==' . $section->section_dcid . ';datedue=ge=' . $today)->count / $plugin->metadata->metadata->schema_table_query_max_page_size);

            for($i = 1; $i <= $pages; $i++){
                $assignments = $plugin->request('get', "/ws/schema/table/pgassignments/?q=sectionid==$section->section_dcid;datedue=ge=$today&page=$i&pagesize=" . $plugin->metadata->metadata->schema_table_query_max_page_size . "&projection=ID,AssignmentID,Name,Abbreviation,PGCategoriesID,DateDue,PointsPossible,Weight,Description");
                foreach($assignments->record as $assignment){
                    if(!$DB->record_exists('powerschool_assignment',array('assignmentid'=>$assignment->id))){
                        $new_assignments[$assignment->id] = $assignment->tables->pgassignments->name;
                    }
                }
            }

        }elseif($section->gradebooktype == 2){
            $assignments = $plugin->request('get', "/ws/xte/section/assignment?users_dcid=$section->teacher_user_dcid&section_ids=$section->section_id");
            foreach($assignments as $assignment){
                if(!$DB->record_exists('powerschool_assignment',array('assignmentid'=>$assignment->assignmentid))){
                    $new_assignments[$assignment->assignmentid] = $assignment->_assignmentsections[0]->name;
                }
            }
        }

        if(!empty($new_assignments)){
            if(isset($section->group_mode))
                $mform->addElement('header', 'moodle', get_string('sync_new_ps_moodle_group','local_powerschool',$section->name));
            else
                $mform->addElement('header', 'moodle', get_string('sync_new_ps_moodle','local_powerschool'));

            foreach($new_assignments as $assignmentid=>$assignment_name){
                $mform->addElement('select', 'new_from_ps['.$section->section_id.']['.$assignmentid.']', $assignment_name, $this->modules);
            }
        }

        return;
    }

    private function get_sync_form_elements($ass){
        global $DB;
        $mform = $this->_form;
        $plugin = $this->plugin;
        $cm = $this->modinfo->get_cm($ass->cmid);
        $sections = array();

        if($plugin->enable_groups){
                $groups = $plugin->get_module_groups(json_decode($cm->availability),$cm);
            if(!empty($groups))
                $sections = $DB->get_records_sql('SELECT cat.courseid as psection, cf.gradebooktype, cf.teacher_user_dcid, cf.term_id, g.name
                                                    FROM {powerschool_course_cat} cat
                                                      LEFT JOIN {powerschool_course_fields} cf ON cf.courseid=cat.courseid_group AND cf.section_id=cat.courseid
                                                      LEFT JOIN {groups} g ON g.id=cat.catid
                                                  WHERE catid IN ('.implode(',',$groups).')');
        }else{
            $sections[] = $DB->get_record_sql('SELECT c.psection, cf.gradebooktype, cf.teacher_user_dcid, cf.term_id, cou.fullname as name
                                                FROM {powerschool_courses} c
                                                  LEFT JOIN {powerschool_course_fields} cf ON cf.courseid=c.mcourse
                                                  LEFT JOIN {course} cou ON cou.id=c.mcourse
                                                WHERE c.mcourse=:mcourse',array('mcourse'=>$ass->courseid));
        }

        if(empty($sections))
            return true;

        $status = ($ass->sync)?html_writer::span(get_string('yes'),'alert alert-success'):html_writer::span(get_string('no'),'alert');
        $mform->addElement('header', 'moodle', $cm->name);
        $mform->addElement('advcheckbox', 'mod_sync['.$ass->id.']', get_string('sync','local_powerschool'), $status, array('group' => 1), array(0, 1));
        foreach($sections as $section){
            if($section->gradebooktype != 2)
                continue;

            $mform->addElement('select', 'mod_cat['.$ass->cmid.']['.$section->psection.']', $section->name, $this->ps_categoties[$section->teacher_user_dcid]);
            if($record = $DB->get_record('powerschool_assignment_cat',array('section_id'=>$section->psection,'moodle_mod'=>$ass->cmid))){
                $mform->setDefault('mod_cat['.$ass->cmid.']['.$section->psection.']', $record->power_cat);
            }
        }

        $mform->setDefault('mod_sync['.$ass->id.']', $ass->sync);

        return true;
    }

}


