<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * PowerSchool Settings
 *
 * @package    local_powerschool
 * @copyright  IntelliBoard, Inc <https://intelliboard.net/>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require_once('classes/Courses.php');
require_once('associate_category_form.php');

$course = optional_param('course', 0, PARAM_INT);

require_login($course);
$context = context_course::instance($course);
require_capability('local/powerschool:view', $context);

$plugin = new Courses();

$PAGE->set_url(new moodle_url("/local/powerschool/associate-category.php",array('course'=>$course)));
$PAGE->set_pagelayout('admin');
$PAGE->set_context($context);
$PAGE->set_title(get_string('powerschoolroot', 'local_powerschool').': '.get_string('associate_category', 'local_powerschool'));
$PAGE->set_heading(get_string('powerschoolroot', 'local_powerschool').': '.get_string('associate_category', 'local_powerschool'));

$form = new associate_category_form(new moodle_url($PAGE->url),array('courseid'=>$course,'instance'=>$plugin));

if ($data = $form->get_data()) {
   unset($data->submitbutton);
    foreach($data as $item=>$value){
        if(empty($value))
            continue;
        list($categoryid, $section_id) = explode('_', $item);
        if($record = $DB->get_record('powerschool_assignment_cat',array('section_id'=>$section_id,'power_cat'=>$categoryid))){
            $record->moodle_mod = $value;
            $DB->update_record('powerschool_assignment_cat',$record);
        }else{
            $record = new stdClass();
            $record->section_id = $section_id;
            $record->power_cat = $categoryid;
            $record->moodle_mod = $value;
            $record->timecreate = time();
            $DB->insert_record('powerschool_assignment_cat',$record,false);
        }
    }
    redirect(new moodle_url($PAGE->url),get_string('updated','moodle',''),0);
}

echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('associate_category_full', 'local_powerschool'));

if($DB->record_exists('powerschool_courses',array('mcourse'=>$course))){
    $form->display();
}else{
    echo html_writer::div(html_writer::span(get_string('course_not_sync','local_powerschool')), 'alert alert-error');
}

echo $OUTPUT->footer();
