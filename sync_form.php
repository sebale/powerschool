<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * PowerSchool Settings
 *
 * @package    local_powerschool
 * @copyright  IntelliBoard, Inc <https://intelliboard.net/>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    //  It must be included from a Moodle page.
}

require_once($CFG->libdir.'/formslib.php');
require_once($CFG->libdir.'/coursecatlib.php');
require_once('classes/Users.php');

class sync_users_form extends moodleform {

    /**
     * Define the form.
     */
    public function definition () {
		global $plugin;
		$params = json_decode(get_config('local_powerschool', 'sync_params'));
        //$params->sync_courses = (array)$params->sync_courses;

        $expansions_student = $expansions_staff = array();
        foreach($plugin->school_ids as $school_id){
            $data = (array)$plugin->request('get', "/ws/v1/school/{$school_id}/student?page=1&pagesize=1")->students;
            $data['@expansions'] = (!empty($data['@expansions']))?str_replace(', fees', '', $data['@expansions']):''; // do not include fees, maibe nex time
            $expansions_arr = (!empty($data['@expansions']))?explode(',', str_replace(' ', '', $data['@expansions'])):array();
            $expansions_student = array_merge($expansions_student,$expansions_arr);

            $data = (array)$plugin->request('get', "/ws/v1/school/{$school_id}/staff?page=1&pagesize=1")->staffs;
            $data['@expansions'] = (!empty($data['@expansions']))?str_replace(', school_affiliations', '', $data['@expansions']):''; // do not include school_affiliations, maybe nex time
            $expansions_arr = (!empty($data['@expansions']))?explode(',', str_replace(' ', '', $data['@expansions'])):array();
            $expansions_staff = array_merge($expansions_staff,$expansions_arr);
        }
        $expansions_student = array_unique($expansions_student);
        $expansions_staff = array_unique($expansions_staff);

        $mform = $this->_form;

		if(!isset($params->section_type)) {
			$mform->addElement('header', 'moodle', get_string('section_setting', 'local_powerschool'));
			$types = array('groups' => get_string('groups', 'local_powerschool'), 'course' => get_string('course', 'local_powerschool'));
			$section_type = $mform->addElement('select', 'section_type', get_string('section_type', 'local_powerschool'), $types);
			$section_type->setSelected('course');
			$mform->addHelpButton('section_type', 'section_type', 'local_powerschool');
		}else{
			$mform->addElement('hidden', 'section_type', $params->section_type);
			$mform->setType('section_type', PARAM_RAW);
		}


        $mform->addElement('header', 'moodle', get_string('students', 'local_powerschool'));
		$mform->addElement('checkbox', 'students', get_string('include'));
		foreach($expansions_student as $item){
			$mform->addElement('checkbox', 'student_expansions_'.$item, get_string($item, 'local_powerschool'));
		}
		
        $mform->addElement('header', 'moodle', get_string('staffs', 'local_powerschool'));
		$mform->addElement('checkbox', 'staffs', get_string('include'));
		foreach($expansions_staff as $item){
			$mform->addElement('checkbox', 'staff_expansions_'.$item, get_string($item, 'local_powerschool'));
		}

		$mform->addElement('header', 'moodle', get_string('users_role', 'local_powerschool'));
		$roles = role_fix_names(get_all_roles());
		$roles_arr = array();
		foreach($roles as $role){
			$roles_arr[$role->id] = $role->localname;
		}
		$student = $mform->addElement('select', 'student_role', get_string('student_role', 'local_powerschool'), $roles_arr);
		$student->setSelected(5);

		$teacher = $mform->addElement('select', 'teacher_role', get_string('co_teacher_role', 'local_powerschool'), $roles_arr);
		$teacher->setSelected(4);

		$teacher = $mform->addElement('select', 'teacher_lead_role', get_string('lead_teacher_role', 'local_powerschool'), $roles_arr);
		$teacher->setSelected(3);


        $categories_obj = coursecat::get(0)->get_children();
        $categories = array();
        foreach($categories_obj as $category){
            $categories[$category->id] = $category->get_formatted_name();
        }
        $school_names = array();
        $mform->addElement('header', 'moodle', get_string('school_category', 'local_powerschool'));
        foreach($plugin->school_ids as $school_id){
            $response = $plugin->request('get','/ws/v1/school/'.$school_id);
            $school_names[$school_id] = $response->school->name;

            $mform->addElement('select', 'category_school_'.$response->school->school_number, $response->school->name, $categories);
        }

        foreach($school_names as $school_id=>$school_name){
            $mform->addElement('header', 'moodle', get_string('terms_sync_per_school', 'local_powerschool',$school_name));

            $school_ids = array_flip($plugin->school_ids);
            $data = new stdClass();
            $data->schoolid = $school_ids[$school_id];
            $data = json_encode($data);

            $count = $plugin->request('post',"/ws/schema/query/get.school.terms/count",array(),$data);
            $pages = (isset($count->count))?ceil($count->count/$plugin->metadata->metadata->schema_table_query_max_page_size):0;
            for($i=1;$i<=$pages;$i++){
                $request = $plugin->request('post',"/ws/schema/query/get.school.terms?page={$i}&pagesize=" . $plugin->metadata->metadata->schema_table_query_max_page_size,array(),$data);
                //print_object($request);

                if(is_array($request->record)){
                    foreach($request->record as $record){
                        $record = $record->tables->terms;
                        $mform->addElement('checkbox', 'terms_'.$school_id.'_'.$record->term_id, $record->terms_abbreviation, '('.$record->terms_firstday.' - '.$record->terms_lastday.')');

                        if(isset($params->sync_terms->{$school_id}->{$record->term_id}))
                            $mform->setDefault('terms_'.$school_id.'_'.$record->term_id, 1);
                    }
                }else{
                    $record = $request->record->tables->terms;
                    $mform->addElement('checkbox', 'terms_'.$school_id.'_'.$record->term_id, $record->terms_abbreviation, '('.$record->terms_firstday.' - '.$record->terms_lastday.')');

                    if(isset($params->sync_terms->{$school_id}->{$record->term_id}))
                        $mform->setDefault('terms_'.$school_id.'_'.$record->term_id, 1);
                }
            }


            $mform->addElement('header', 'moodle', get_string('courses_sync_per_school', 'local_powerschool',$school_name));

            $count = $plugin->request('get',"/ws/v1/school/{$school_id}/course/count");
            $pages = (isset($count->resource->count))?ceil($count->resource->count/$plugin->metadata->metadata->course_max_page_size):0;
            for($i=1;$i<=$pages;$i++){
                $request = $plugin->request('get', "/ws/v1/school/{$school_id}/course?page={$i}&pagesize=" . $plugin->metadata->metadata->course_max_page_size);

                if(is_array($request->courses->course)){
                    foreach($request->courses->course as $course){
                        $clean_course_number = str_replace(' ', '', $course->course_number);
                        $mform->addElement('checkbox', 'courses_'.$clean_course_number, $course->course_name, '('.$course->course_number.')');

                        if(isset($params->sync_courses->{$clean_course_number}))
                            $mform->setDefault('courses_'.$clean_course_number, 1);
                    }
                }else{
                    $course = $request->courses->course;
                    $clean_course_number = str_replace(' ', '', $course->course_number);
                    $mform->addElement('checkbox', 'courses_'.$clean_course_number, $course->course_name, '('.$course->course_number.')');

                    if(isset($params->sync_courses->{$clean_course_number}))
                        $mform->setDefault('courses_'.$clean_course_number, 1);
                }


            }
        }

        //$this->add_action_buttons(false, get_string('sync', 'local_powerschool'));
        $buttonarray=array();
        $buttonarray[] = &$mform->createElement('submit', 'save', get_string('save', 'local_powerschool'));
        $buttonarray[] = &$mform->createElement('submit', 'save_sync_all', get_string('save_sync_all', 'local_powerschool'));
        $buttonarray[] = &$mform->createElement('submit', 'save_sync_users', get_string('save_sync_users', 'local_powerschool'));
        $buttonarray[] = &$mform->createElement('submit', 'save_sync_courses', get_string('save_sync_courses', 'local_powerschool'));

        $mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
        $mform->closeHeaderBefore('buttonar');

		$this->set_data($params);
    }


    public function validation($params, $files) {
		$errors = parent::validation($params, $files);
		if(!isset($params['students']) && !isset($params['staffs'])){
			$errors['students'] = get_string('must_exist_students_or_staffs', 'local_powerschool');
			$errors['staffs'] = get_string('must_exist_students_or_staffs', 'local_powerschool');
		}
		
        return $errors;
    }
}


